# SkyLight
一个基于Catserver魔改的多线程服务端，目前对部分mod以及已知部分修改过pathfinding的mod不兼容
# Build
克隆此项目，打开终端，运行./gradlew setup genPatches build
# 下载
可以去CI那里下载(目前CI没了，最新的提交构建不了
emmm.目前在我的oss上有一份(
链接:https://aoawfiles.mc66.club/SkyLight-Reboot-ac60a0a-universal.jar
# 功能
1.异步+并行实体&方块实体更新\
2.并行随机刻\
3.并行EntityTracker\
4.并行世界更新\
5.异步刷怪
